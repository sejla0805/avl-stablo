#ifndef STABLO_H_INCLUDED
#define STABLO_H_INCLUDED
#include <iostream>
#include <vector>
#include <random>
using namespace std;

class Stablo {
private:
    struct Cvor {
        Cvor *ld, *dd, *rod;
        int element;
        double prioritet;
        Cvor (int element, double prioritet, Cvor *rod=nullptr, Cvor *ld=nullptr, Cvor *dd=nullptr):
            element(element), prioritet(prioritet), rod(rod), ld(ld), dd(dd){}
    };

private:
    Cvor *korijen;
    int velicina;
    void InOrder (Cvor *tekuci, void (*akcija)(int&));


public:
    Stablo() {korijen=nullptr; velicina=0;}
    Stablo(int element) {korijen = new Cvor(element, rand()/(RAND_MAX/1.)); velicina=1;}
    Stablo (int element, double prioritet) {korijen=new Cvor (element, prioritet, nullptr); velicina=1;}
    Stablo (Cvor *pok) {korijen=pok;}

    void Umetni (int element);
    void Umetni (int element, double prioritet);
    Cvor* Pronadji (int element);
    friend pair<Stablo, Stablo> Razdvoji (Stablo &s, int vrijednos);
    void Obrisi (Cvor *x);
    Cvor *Obrisi (Cvor *k, int e);

    void InOrder (void(*akcija)(int&));
    Cvor* Spoji (Cvor *korijen1, Cvor *korijen2);
    void RotacijaULijevo (Cvor **x);
    void RotacijaUDesno (Cvor **x);
};

#include "stablo.cpp"
#endif // STABLO_H_INCLUDED

