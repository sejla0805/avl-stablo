#ifndef STABLO_CPP
#define STABLO_CPP
#include "stablo.h"
#include <iostream>
#include <queue>
using namespace std;


void Stablo::Umetni (int element){
    if (!korijen){
        korijen = new Cvor(element, rand()/(RAND_MAX/1.));
        velicina = 1;
        return;
    }
    Cvor *trenutni = korijen, *prethodni = nullptr;
    while (trenutni != nullptr){
        if (element > trenutni->element){
            prethodni = trenutni;
            trenutni = trenutni->dd;
        }
        else if (element < trenutni->element){
            prethodni = trenutni;
            trenutni = trenutni->ld;
        }
        else return;
    }
    if (element > prethodni->element){
        prethodni -> dd = new Cvor(element, rand()/(RAND_MAX/1.), prethodni);
        if (prethodni->dd->prioritet > prethodni->prioritet){
            Cvor *pom =prethodni->dd;
            pom -> rod = prethodni -> rod;
            if (prethodni -> rod -> ld == prethodni && prethodni -> rod != nullptr)
                prethodni -> rod -> ld = pom;
            else if (prethodni -> rod -> dd == prethodni && prethodni -> rod != nullptr)
                prethodni -> rod -> dd = pom;
            else
                korijen = pom;
            pom -> ld = prethodni;
            prethodni -> rod = pom;


            while (pom->prioritet < pom->rod->prioritet && pom->rod != nullptr){
                prethodni = pom -> rod;
                if (pom->rod->dd == pom){
                    pom -> rod = prethodni -> rod;
                    if (prethodni -> rod -> ld == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> ld = pom;
                    else if (prethodni -> rod -> dd == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> dd = pom;
                    else
                        korijen = pom;

                    if (pom->ld != nullptr){
                        prethodni->dd = pom->ld;
                    }
                    pom -> ld = prethodni;
                    prethodni -> rod = pom;
                }
                else {
                     pom -> rod = prethodni -> rod;
                    if (prethodni -> rod -> dd == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> dd = pom;
                    else if (prethodni -> rod -> ld == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> ld = pom;
                    else
                        korijen = pom;

                    if (pom->dd != nullptr){
                        prethodni->ld = pom->dd;
                    }
                    pom -> dd = prethodni;
                    prethodni -> rod = pom;
                }
            }
        }
       }

    else{
        prethodni -> ld = new Cvor(element, rand()/(RAND_MAX/1.), prethodni);
        if (prethodni->ld->prioritet > prethodni->prioritet){
            Cvor *pom =prethodni->ld;
            pom -> rod = prethodni -> rod;
            if (prethodni -> rod -> dd == prethodni && prethodni -> rod != nullptr)
                prethodni -> rod -> dd = pom;
            else if (prethodni -> rod -> ld == prethodni && prethodni -> rod != nullptr)
                prethodni -> rod -> ld = pom;
            else
                korijen = pom;
            pom -> dd = prethodni;
            prethodni -> rod = pom;


            while (pom->prioritet < pom->rod->prioritet && pom->rod != nullptr){
                prethodni = pom -> rod;
                if (pom->rod->dd == pom){
                    pom -> rod = prethodni -> rod;
                    if (prethodni -> rod -> ld == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> ld = pom;
                    else if (prethodni -> rod -> dd == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> dd = pom;
                    else
                        korijen = pom;

                    if (pom->ld != nullptr){
                        prethodni->dd = pom->ld;
                    }
                    pom -> ld = prethodni;
                    prethodni -> rod = pom;
                }
                else {
                     pom -> rod = prethodni -> rod;
                    if (prethodni -> rod -> dd == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> dd = pom;
                    else if (prethodni -> rod -> ld == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> ld = pom;
                    else
                        korijen = pom;

                    if (pom->dd != nullptr){
                        prethodni->ld = pom->dd;
                    }
                    pom -> dd = prethodni;
                    prethodni -> rod = pom;
                }


            }
        }
    }
    velicina++;
    return;
}


void Stablo::Umetni (int element, double prioritet){
    if (!korijen){
        korijen = new Cvor(element, prioritet, nullptr);
        velicina = 1;
        return;
    }
    Cvor *trenutni = korijen, *prethodni = nullptr;
    while (trenutni != nullptr){
        if (element > trenutni->element){
            prethodni = trenutni;
            trenutni = trenutni->dd;
        }
        else if (element < trenutni->element){
            prethodni = trenutni;
            trenutni = trenutni->ld;
        }
        else return;
    }
    if (element > prethodni->element){
        prethodni -> dd = new Cvor(element, prioritet, prethodni);
        if (prethodni->dd->prioritet > prethodni->prioritet){
            Cvor *pom =prethodni->dd;
            pom -> rod = prethodni -> rod;
            if (prethodni -> rod -> ld == prethodni && prethodni -> rod != nullptr)
                prethodni -> rod -> ld = pom;
            else if (prethodni -> rod -> dd == prethodni && prethodni -> rod != nullptr)
                prethodni -> rod -> dd = pom;
            else
                korijen = pom;
            pom -> ld = prethodni;
            prethodni -> rod = pom;


            while (pom->prioritet < pom->rod->prioritet && pom->rod != nullptr){
                prethodni = pom -> rod;
                if (pom->rod->dd == pom){
                    pom -> rod = prethodni -> rod;
                    if (prethodni -> rod -> ld == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> ld = pom;
                    else if (prethodni -> rod -> dd == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> dd = pom;
                    else
                        korijen = pom;

                    if (pom->ld != nullptr){
                        prethodni->dd = pom->ld;
                    }
                    pom -> ld = prethodni;
                    prethodni -> rod = pom;
                }
                else {
                     pom -> rod = prethodni -> rod;
                    if (prethodni -> rod -> dd == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> dd = pom;
                    else if (prethodni -> rod -> ld == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> ld = pom;
                    else
                        korijen = pom;

                    if (pom->dd != nullptr){
                        prethodni->ld = pom->dd;
                    }
                    pom -> dd = prethodni;
                    prethodni -> rod = pom;
                }
            }
        }
       }

    else{
        prethodni -> ld = new Cvor(element, prioritet, prethodni);
        if (prethodni->ld->prioritet > prethodni->prioritet){
            Cvor *pom =prethodni->ld;
            pom -> rod = prethodni -> rod;
            if (prethodni -> rod -> dd == prethodni && prethodni -> rod != nullptr)
                prethodni -> rod -> dd = pom;
            else if (prethodni -> rod -> ld == prethodni && prethodni -> rod != nullptr)
                prethodni -> rod -> ld = pom;
            else
                korijen = pom;
            pom -> dd = prethodni;
            prethodni -> rod = pom;


            while (pom->prioritet < pom->rod->prioritet && pom->rod != nullptr){
                prethodni = pom -> rod;
                if (pom->rod->dd == pom){
                    pom -> rod = prethodni -> rod;
                    if (prethodni -> rod -> ld == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> ld = pom;
                    else if (prethodni -> rod -> dd == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> dd = pom;
                    else
                        korijen = pom;

                    if (pom->ld != nullptr){
                        prethodni->dd = pom->ld;
                    }
                    pom -> ld = prethodni;
                    prethodni -> rod = pom;
                }
                else {
                     pom -> rod = prethodni -> rod;
                    if (prethodni -> rod -> dd == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> dd = pom;
                    else if (prethodni -> rod -> ld == prethodni || prethodni -> rod != nullptr)
                        prethodni -> rod -> ld = pom;
                    else
                        korijen = pom;

                    if (pom->dd != nullptr){
                        prethodni->ld = pom->dd;
                    }
                    pom -> dd = prethodni;
                    prethodni -> rod = pom;
                }
            }
        }
    }
    velicina++;
    return;
}

void Stablo::InOrder (void (*akcija)(int&)){
    InOrder(korijen, akcija);
}

void Stablo::InOrder (Cvor *tekuci, void (*akcija)(int&)){
    if (tekuci){
        InOrder(tekuci->ld, akcija);
        akcija(tekuci->element);
        InOrder(tekuci->dd, akcija);
    }
}

Stablo::Cvor* Stablo::Pronadji (int element){
    Cvor *pom = korijen;
    while (true){
        if (pom == nullptr) break;

        if (pom->element == element)
            return pom;
        else if (pom->element > element)
            pom = pom -> ld;
        else
            pom = pom -> dd;
    }
    return nullptr;
}


pair<Stablo, Stablo> Razdvoji (Stablo &s, int vrijednost){
    pair<Stablo, Stablo> pom;
    s.Umetni(vrijednost, 1.5);
    Stablo manje_stablo(s.korijen->ld);
    Stablo vece_stablo(s.korijen->dd);
    pom.first=manje_stablo;
    pom.second=vece_stablo;
    return pom;
}


void Stablo::RotacijaULijevo (Cvor **x){
    Cvor *pom1 = *x;
    Cvor *pom2 = pom1->dd;
    pom1->dd = pom2->ld;
    pom2->ld = pom1;
    *x = pom2;
}


void Stablo::RotacijaUDesno (Cvor **x){
    Cvor *pom1 = *x;
    Cvor *pom2 = pom1->ld;
    pom1->ld = pom2->dd;
    pom2->dd = pom1;
    *x = pom2;
}


void Stablo::Obrisi (Cvor *x){
    if (x->dd==nullptr && x->ld==nullptr){
        //Brisanje lista
       if (x == korijen){
            korijen == nullptr;
            return;
       }
       if (x->rod->ld == x){
            x->rod->ld = nullptr;
            delete x;
       }
       else {
            x->rod->dd = nullptr;
            delete x;
       }
    }

    else if (x->ld!=nullptr && x->dd!=nullptr){
        //Brisanje cvora koji ima dvoje djece
        Cvor *pom1;
        Cvor *pom2;
        if (x == korijen){
            pom1 = x->ld->ld;
            pom2 = x->ld->dd;
            korijen = x->ld;
            x->ld->rod = nullptr;
            korijen->ld = pom1;
            pom1->rod = korijen->ld;
            //while ( ){}
        }
    }

    else {
        //Brisanje cvora koji ima jedno dijete
        if (x == korijen){
            if (x->ld == nullptr){
                x->dd->rod = nullptr;
                korijen = x->dd;
            }
            else {
                x->ld->rod = nullptr;
                korijen = x->ld;
            }
            delete x;
            return;
        }
        if (x->rod->ld == nullptr){
            Cvor *pom = x->dd;
            if (x->rod->dd == x){
                x->rod->dd = pom;
                pom->rod = x->rod->dd;
            }
            else {
                x->rod->ld = pom;
                pom->rod = x->rod->ld;
            }
        }
        else {
            Cvor *pom = x->ld;
            if (x->rod->dd == x){
                x->rod->dd = pom;
                pom->rod = x->rod->dd;
            }
            else {
                x->rod->ld = pom;
                pom->rod = x->rod->ld;
            }
        }
        delete x;
    }
}

Stablo::Cvor* Stablo::Spoji (Cvor *korijen1, Cvor *korijen2){
    if (!korijen1) return korijen2;
    if (!korijen2) return korijen1;
    if (korijen1->prioritet > korijen2->prioritet){
        korijen1->dd = Spoji(korijen1->dd, korijen2);
        return korijen1;
    }
    else {
        korijen2->ld = Spoji(korijen1, korijen2->ld);
        return korijen2;
    }
}


Stablo::Cvor* Stablo::Obrisi (Cvor *k, int e){
    if (!k) return k;
    if (k->element == e){
        Cvor *pom = Spoji(k->ld, k->dd);
        delete k;
        return pom;
    }
    else if (k->element > e)
        k->ld = Obrisi(k->ld, e);
    else
        k->dd = Obrisi(k->dd, e);
    return k;
}

#endif // STABLO_CPP
